class CreateReclamacaos < ActiveRecord::Migration
  def change
    create_table :reclamacaos do |t|
      t.references :usuario
      t.string :titulo
      t.text :descricao

      t.timestamps
    end
    add_index :reclamacaos, :usuario_id
  end
end
