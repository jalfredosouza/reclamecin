class CreateOpiniaos < ActiveRecord::Migration
  def change
    create_table :opiniaos do |t|
      t.references :usuario
      t.boolean :gostou
      t.boolean :nao_gostou

      t.timestamps
    end
    add_index :opiniaos, :usuario_id
  end
end
