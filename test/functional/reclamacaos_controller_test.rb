require 'test_helper'

class ReclamacaosControllerTest < ActionController::TestCase
  setup do
    @reclamacao = reclamacaos(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:reclamacaos)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create reclamacao" do
    assert_difference('Reclamacao.count') do
      post :create, reclamacao: { descricao: @reclamacao.descricao, titulo: @reclamacao.titulo }
    end

    assert_redirected_to reclamacao_path(assigns(:reclamacao))
  end

  test "should show reclamacao" do
    get :show, id: @reclamacao
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @reclamacao
    assert_response :success
  end

  test "should update reclamacao" do
    put :update, id: @reclamacao, reclamacao: { descricao: @reclamacao.descricao, titulo: @reclamacao.titulo }
    assert_redirected_to reclamacao_path(assigns(:reclamacao))
  end

  test "should destroy reclamacao" do
    assert_difference('Reclamacao.count', -1) do
      delete :destroy, id: @reclamacao
    end

    assert_redirected_to reclamacaos_path
  end
end
