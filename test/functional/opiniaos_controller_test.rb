require 'test_helper'

class OpiniaosControllerTest < ActionController::TestCase
  setup do
    @opiniao = opiniaos(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:opiniaos)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create opiniao" do
    assert_difference('Opiniao.count') do
      post :create, opiniao: { gostou: @opiniao.gostou, nao_gostou: @opiniao.nao_gostou }
    end

    assert_redirected_to opiniao_path(assigns(:opiniao))
  end

  test "should show opiniao" do
    get :show, id: @opiniao
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @opiniao
    assert_response :success
  end

  test "should update opiniao" do
    put :update, id: @opiniao, opiniao: { gostou: @opiniao.gostou, nao_gostou: @opiniao.nao_gostou }
    assert_redirected_to opiniao_path(assigns(:opiniao))
  end

  test "should destroy opiniao" do
    assert_difference('Opiniao.count', -1) do
      delete :destroy, id: @opiniao
    end

    assert_redirected_to opiniaos_path
  end
end
