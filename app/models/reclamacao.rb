class Reclamacao < ActiveRecord::Base
  belongs_to :usuario
  has_many :opiniaos
  attr_accessible :descricao , :titulo, :usuario_id
  validates :descricao, presence: true
  validates :titulo, presence: true
end
