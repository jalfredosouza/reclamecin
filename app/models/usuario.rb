class Usuario < ActiveRecord::Base
  has_many :opiniaos
  has_many :reclamacaos
  attr_accessible :email, :login, :senha
  def to_s
    login
  end
end
