class Opiniao < ActiveRecord::Base
  belongs_to :usuario
  belongs_to :reclamacao
  attr_accessible :gostou, :nao_gostou, :usuario_id, :reclamacao_id
end
