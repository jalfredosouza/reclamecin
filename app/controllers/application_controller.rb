class ApplicationController < ActionController::Base
  protect_from_forgery

  before_filter :logado

  def logado
    unless session[:sis_logon]
      flash[:notice] = 'Voce precisa logar no sistema para realizar esta acao!'
      redirect_to :controller => 'login', :action => 'new'
    end
  end
end
