
class CadastrarReprovacaoController < ApplicationController
  def create
    @reprovacao = Opiniao.where(:usuario_id => session[:sis_logon].id, :reclamacao_id => params[:id]).first
    @reclamacao = Reclamacao.find(params[:id])
    unless @reprovacao
      Opiniao.create(:usuario_id => session[:sis_logon].id, :reclamacao_id => @reclamacao.id, :nao_gostou => true)
    else
      flash[:notice] = "Este usuario ja opinou esta reclamacao."
    end
    redirect_to @reclamacao
  end
end
