
class OpiniaosController < ApplicationController
  # GET /opiniaos
  # GET /opiniaos.json
  def index
    @opiniaos = Opiniao.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @opiniaos }
    end
  end

  # GET /opiniaos/1
  # GET /opiniaos/1.json
  def show
    @opiniao = Opiniao.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @opiniao }
    end
  end

  # GET /opiniaos/new
  # GET /opiniaos/new.json
  def new
    @opiniao = Opiniao.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @opiniao }
    end
  end

  # GET /opiniaos/1/edit
  def edit
    @opiniao = Opiniao.find(params[:id])
  end

  # POST /opiniaos
  # POST /opiniaos.json
  def create
    @opiniao = Opiniao.new(params[:opiniao])

    respond_to do |format|
      if @opiniao.save
        format.html { redirect_to @opiniao, notice: 'Opiniao was successfully created.' }
        format.json { render json: @opiniao, status: :created, location: @opiniao }
      else
        format.html { render action: "new" }
        format.json { render json: @opiniao.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /opiniaos/1
  # PUT /opiniaos/1.json
  def update
    @opiniao = Opiniao.find(params[:id])

    respond_to do |format|
      if @opiniao.update_attributes(params[:opiniao])
        format.html { redirect_to @opiniao, notice: 'Opiniao was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @opiniao.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /opiniaos/1
  # DELETE /opiniaos/1.json
  def destroy
    @opiniao = Opiniao.find(params[:id])
    @opiniao.destroy

    respond_to do |format|
      format.html { redirect_to opiniaos_url }
      format.json { head :no_content }
    end
  end
end
