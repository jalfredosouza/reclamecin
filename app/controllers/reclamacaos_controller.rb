
class ReclamacaosController < ApplicationController
  # GET /reclamacaos
  # GET /reclamacaos.json
  skip_before_filter :logado
  def index
  
    @reclamacaos = Reclamacao.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @reclamacaos }
    end
  end

  # GET /reclamacaos/1
  # GET /reclamacaos/1.json
  def show
	
    @reclamacao = Reclamacao.find(params[:id])
    @aprova = Opiniao.where(:reclamacao_id => @reclamacao.id, :gostou => true).count
    @aprovados = Opiniao.where(:reclamacao_id => @reclamacao.id, :gostou => true)
    @reprova = Opiniao.where(:reclamacao_id => @reclamacao.id, :nao_gostou => true).count
    @reprovados = Opiniao.where(:reclamacao_id => @reclamacao.id, :nao_gostou => true)
    @opinioes = Opiniao.where(:reclamacao_id => @reclamacao.id)

    respond_to do |format|
      format.html # show.html.erb
      #format.json { render json: @reclamacao.to_json(:include => :opiniaos) }
      format.json { render json: [@reclamacao.to_json, :aprovados => @aprovados.to_json(:only => [:created_at, :usuario_id, :updated_at]) , :reprovados => @reprovados.to_json(:only => [:created_at, :usuario_id, :updated_at])] }
    end
  end

  # GET /reclamacaos/new
  # GET /reclamacaos/new.json
  def new
    @reclamacao = Reclamacao.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @reclamacao }
    end
  end

  # GET /reclamacaos/1/edit
  def edit
    @reclamacao = Reclamacao.find(params[:id])
  end

  # POST /reclamacaos
  # POST /reclamacaos.json
  def create
    @reclamacao = Reclamacao.new(params[:reclamacao])

    respond_to do |format|
      if @reclamacao.save
        format.html { redirect_to @reclamacao, notice: 'Reclamacao was successfully created.' }
        format.json { render json: @reclamacao, status: :created, location: @reclamacao }
      else
        format.html { render action: "new" }
        format.json { render json: @reclamacao.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /reclamacaos/1
  # PUT /reclamacaos/1.json
  def update
    @reclamacao = Reclamacao.find(params[:id])

    respond_to do |format|
      if @reclamacao.update_attributes(params[:reclamacao])
        format.html { redirect_to @reclamacao, notice: 'Reclamacao was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @reclamacao.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /reclamacaos/1
  # DELETE /reclamacaos/1.json
  def destroy
    @reclamacao = Reclamacao.find(params[:id])
    @reclamacao.destroy

    respond_to do |format|
      format.html { redirect_to reclamacaos_url }
      format.json { head :no_content }
    end
  end
end
