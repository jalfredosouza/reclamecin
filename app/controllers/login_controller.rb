
class LoginController < ApplicationController

  skip_before_filter :logado

  def new
  end

  def create
    @usuario = Usuario.where(:login =>  params[:login], :senha => params[:senha]).first

    if @usuario

      session[:sis_logon] = @usuario
      flash[:notice] = 'Logado com sucesso! Bem-vindo(a).'
      redirect_to :controller => "reclamacaos", :action => "index"

    else

      flash[:notice] = 'Verifique suas credenciais para acesso ao sistema.'
      redirect_to :action => "new"

    end
  end

  def destroy
    session[:sis_logon] = nil
    flash[:notice] = 'Desconectado com sucesso!'
    redirect_to :controller => 'login', :action => "new"
  end
end
