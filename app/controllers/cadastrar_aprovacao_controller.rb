
class CadastrarAprovacaoController < ApplicationController
  def create
    @aprovacao = Opiniao.where(:usuario_id => session[:sis_logon].id, :reclamacao_id => params[:id]).first
    @reclamacao = Reclamacao.find(params[:id])
    unless @aprovacao
      Opiniao.create(:usuario_id => session[:sis_logon].id, :reclamacao_id => @reclamacao.id, :gostou => true)
    else
      flash[:notice] = "Este usuario ja opinou esta reclamacao."
    end
    redirect_to @reclamacao
  end
end
